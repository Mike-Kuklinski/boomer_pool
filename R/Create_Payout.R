# Project: Boomer's Pool - Golf Pool Major Tracking
# Name: Create_Payout - NOT USED
# Author: Mike Kuklinski
# Date: 2018-07-02
# Description: Funcction to create estimated payout structure for tournament
# Function
################################################################################

# Testing values
#id_num <- 1013 #US Open 2012
#old_purse <- 8000000


create_payout <- function(id_num, old_purse){
    # Get html
    url <- paste('http://espn.go.com/golf/leaderboard?tournamentId=', id_num, sep = '')
    html_text <- GET(url)
    html_text <- content(html_text, as = 'text')
    parsed_html <- htmlParse(html_text, asText = TRUE)
    # Extract table statistics and header
    pos_header <- xpathSApply(parsed_html, '//*[(@id = "regular-leaderboard")]//th', xmlValue)
    pos_header <- make.names(pos_header)
    pos_table <- xpathSApply(parsed_html, '//*[(@id = "regular-leaderboard")]//td', xmlValue)
    # Form table
    pos_table <- data.frame(matrix(data = pos_table, 
                                   ncol = length(pos_header), 
                                   byrow = T), 
                            stringsAsFactors = F)
    names(pos_table) <- pos_header
    # Subset Table
    pos_table <- pos_table[,c('POS','EARNINGS')]
    # Adjust variables
    pos_table$POS <- as.numeric(gsub("^T", '', pos_table$POS))
    pos_table$EARNINGS <- as.numeric(gsub("[[:punct:]]",'', pos_table$EARNINGS))
    pos_table <- na.exclude(pos_table)
    pos_table <- subset(pos_table, EARNINGS != 0 & POS >= 10)
    con_pos_table <- count(pos_table)
    con_pos_table$freq <- con_pos_table$freq - 1
    con_pos_table$adj_POS <- con_pos_table$POS + con_pos_table$freq/2
    # Create regresssion line
    per_table <- con_pos_table
    per_table$EARNINGS <- per_table$EARNINGS/old_purse
    per_table <- rbind(data.frame(POS = 10, EARNINGS = .027, freq = 1, adj_POS = 10), per_table)
    per_reg <- lm(EARNINGS ~ poly(adj_POS, 6, raw = T), data = per_table)
    fit_points <- data.frame('fit' = per_reg$fitted.values)
    #g <- ggplot(data = per_table, aes(y = EARNINGS, x = adj_POS)) + geom_point()
    #g <- g + geom_point(data = fit_points, aes(y = fit, x = per_table$adj_POS), colour = 'red')
    # Predict continous values for all positions
    pred_fit <- predict(per_reg, newdata = data.frame('adj_POS' = 10:69))
    #g <- g + geom_point(data = data.frame('new' = pred_fit), aes(y = new, x = 10:69), colour = 'green')
    #g
    # Join known percentages for first 10 places
    finish_perc <- c(c(.18, .1080, .068,.048,.04,.036, .0335,.031,.029,.027), pred_fit[-1])
    names(finish_perc) <- NULL
    payout_perc <- data.frame('Rank' = 1:length(finish_perc), 'Payout Perc' = finish_perc)
    write.csv(payout_perc, 'Data/payout_perc.csv', row.names = F)
}

