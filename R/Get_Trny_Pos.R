# Project: Boomer's Pool - Golf Pool Major Tracking
# Name: Get_Trny_Pos
# Author: Mike Kuklinski
# Date: 2018-07-02
# Description: Function which takes an ESPN tourney id and returns the finishes for that tournament

# Function
################################################################################

get_trny_pos <- function(id_num){
    # Set tie_breaker flag
    tb <- FALSE
    # Get html
    url <- paste('http://espn.go.com/golf/leaderboard?tournamentId=', id_num, sep = '')
    html_text <- GET(url)
    html_text <- content(html_text, as = 'text')
    parsed_html <- htmlParse(html_text, asText = TRUE)
    # Extract table header
    pos_header <- xpathSApply(parsed_html, '//th', xmlValue)
    pos_header <- make.names(pos_header)
    pos_header <- c(pos_header, "DEL")
    # Check for tie-breaker and remove extra column headers
    if(identical(pos_header[1:2], c('PLAYER','Hole.18'))){
        pos_header <- pos_header[-c(1,2)]
        tb <- T}
    # Extract player positions
    pos_table <- xpathSApply(parsed_html, '//td', xmlValue)
    # Adjust for tie-break
    if(tb){
        pos_table <- pos_table[grep("1", pos_table)[1]:length(pos_table)]
    }
    # Check for and remove projected cut and cut information
    if(length(grep('(make the cut)|([Pp]rojected)', pos_table)) > 0){
        cut_idx <- grep('(make the cut)|([Pp]rojected)', pos_table)
        proj_cut <- pos_table[cut_idx]
        pci <- regexpr('(-|\\+)?[0-9]{1,}', proj_cut)
        proj_cut_stroke <- substr(proj_cut, pci, (pci + attr(pci, 'match.length') - 1))
        pos_table <- pos_table[-cut_idx]
    }else{
        cut_idx <- NA
    }
    # Form table
    pos_table <- data.frame(matrix(data = pos_table, 
                                   ncol = length(pos_header), 
                                   byrow = T), 
                            stringsAsFactors = F)
    names(pos_table) <- pos_header
    # Adjust Player Names to delete short names
    pos_table$PLAYER <- del_short_name(pos_table$PLAYER)
    # Subset Table
    pos_table <- pos_table[,c('PLAYER',
                              'POS',
                              'THRU',
                              'TO.PAR',
                              'TODAY',
                              'R1',
                              'R2',
                              'R3',
                              'R4',
                              'TOT')]
    # Rename variables
    names(pos_table) <- c('Player', 
                          'Position', 
                          'THRU', 
                          'To Par',
                          'Today',
                          'R1', 
                          'R2', 
                          'R3', 
                          'R4', 
                          'Total')
    # Adjust Rankings
    pos_table$Position <- as.factor(pos_table$Position)
    trny_started <- FALSE
    if((FALSE %in% (pos_table$Total != "--")) == FALSE){trny_started <- TRUE}
    pos_table$`Sort Pos` <- as.numeric(gsub("^T",'', pos_table$Position))
    if(trny_started){
        pos_table$Position <- gsub('-', 'CUT', pos_table$Position)
    }
    # Adjust THRU to account for rounds played
    pos_table$Day_THRU <- pos_table$THRU
    pos_table$Day_THRU <- gsub("^$","0", pos_table$Day_THRU)
    for(idx in 1:nrow(pos_table)){
        tmp_THRU <- pos_table[idx, 'THRU']
        if(tmp_THRU == 'F' | tmp_THRU == ''){tmp_THRU <- '0'}
        if(grepl('[0-9]', tmp_THRU)){
            num_rnds <- length(grep('[0-9]', pos_table[idx,c('R1', 'R2', 'R3', 'R4')])) 
            pos_table[idx,'THRU'] <-num_rnds*18 + as.integer(tmp_THRU)
        }
    }
    # Re-sort Table
    pos_table <- pos_table[,c('Player',
                              'Position',
                              'Sort Pos', 
                              'Day_THRU',
                              'To Par',
                              'Today',
                              'R1',
                              'R2',
                              'R3',
                              'R4',
                              'THRU',
                              'Total')]
    pos_table
}

